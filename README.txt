Answers
Austin Pinderski


In reference to the @Before methods of the TestList and TestIterator classes,
it is possible to substitute ArrayList with Linked List. There is no behavioral
difference.This is because both of these are under the same interface "List."

In testRemove() in TestIterator, if one writes list.remove(Integer.valueOf(77)),
a ConcurrentModificationException will occur. This happens because when iterating
over a collection, iterator.remove() is the only legal way to remove.
Modifying a collection while iterating in another way will cause errors.

In testRemoveObject() in TestList, list.remove(5) removes the 5th element of the list.
In the same method, list.remove(Integer.valueOf(5)); removes the first instance
in the list where the value is 5.

In regards to the TestPerformance class, after changing variable SIZE to larger and
larger numbers, I determined that there was far less runtime for accessing in
ArrayList than LinkedList, and far less runtime in AddRemove for LinkedList
compared to ArrayList. For example, for SIZE = 100000, testLinkedListAccess was 57
seconds, while testArrayListAccess was only 25 ms. Also, testArrayListAddRemove was
20 seconds, while testLinkedListAddRemove was 54ms.
